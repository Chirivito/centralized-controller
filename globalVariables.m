global sizeMap Obstacles robotList numRob requestMatrix step_t set_t travel_t comm_t 
% All times are given in miliseconds

sizeMap = 20;   

Obstacles = [5 5 4 12; 5 13 4 12]; % First row represents obstacle 1, 2nd row obstacle 2,...
% Obstacles = [i-th row    j-th column    width   height]

% robotList = [2 2 1 1; 15 3 1 1; 2 15 0 1; 18 18 0 1]%; 2 10 3 1; 19 8 2 1];   % Simulation 1
robotList = [2 2 1 1; 15 3 1 1; 18 10 0 1; 18 18 0 1]%; 2 10 3 1; 19 8 2 1];   % Simulation 2
% [i-th row, j-th column, orientation, availability]
numRob = size(robotList,1); % Number of robots

% requestMatrix = [10 10; 8 4]%; 6 19];   % Simulation 1
requestMatrix = [14 18; 3 3]%; 19 10];   % Simulation 2

step_t = 1;       % Step time  
set_t = 600000;   % Overall simulation duration
travel_t = 500;   % Travel_t is time to travel one step, is 0.5 based on robot speed (500)
comm_t = 60;      % Communication time (60)
