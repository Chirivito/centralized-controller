% Script for the centralized controller 
% Convention: i is the vertical direction (row), j is the horizontal 
% direction (column)

% Clear workspace and close all figures
clear
close all
clc

% Run script globalVariables.m to initialize the global parameters used to
% compare the centralized and decentralized controller
globalVariables

%% Create the map
global a;          % a is a matrix the size of the map, which is used to place the obstacles and to compute the weight matrix
a = zeros(sizeMap); % Initialize a

% Place the first obstacle in the global variable a
i = Obstacles(1,1);
j = Obstacles(1,2);
width = Obstacles(1,3); 
height = Obstacles(1,4);
placeObstacle(sizeMap,i,j,width,height);

% Place the second obstacle in the global variable a
i = Obstacles(2,1);
j = Obstacles(2,2);
width = Obstacles(2,3);
height = Obstacles(2,4);
placeObstacle(sizeMap, i, j, width, height);

%% Set up parameters for while-loop
tot_t = 0;          % Total running simulation time so far
isReq = 1;
taskNum = 1;        % Task Number

% Check time
multiple_check_t = check_t/step_t;
counter_check_t = 0;    % Initialize the counter
triggerComputation = 0;

% Computation time
multiple_comp_t = comp_t/step_t;
counter_comp_t = 0;     % Initialize the counter
triggerCommunication = 0;

% Communication time
multiple_comm_t = comm_t/step_t;
counter_comm_t = 0;
triggerTravel = zeros(numRob,1);

% Travel time
multiple_travel_t = travel_t/step_t;
counter_travel_t = zeros(numRob,1);   % Initialize the counter to zero
counter_path = ones(numRob,1);   % Keeps track of how many steps the selected robot has moved according to its computed path. The row number represents the robot ID.

currentAssignedTaskMatrix = cell(numRob,2);    % This matrix contains the current path assigned to each robot
for i = 1:numRob
    currentAssignedTaskMatrix(i,:) = {zeros(100,2),NaN};
%          = {path robot 1, task number;
%             path robot 2, task number;
%                 ...           ...
%     path robot numberRob, task number}
end

historyRobotsPositions = num2cell(NaN(set_t,numRob)); % The rows of this matrix represent the time and the columns belong to the corresponding robot. The cells contain the position of robot j at the time i.

%% Time loop
while tot_t ~= set_t
    
    if isReq == 1        
        % Check time
        if counter_check_t == multiple_check_t  % Alternatively use expression like counter_check_t == n*multiple_check_t such that the counter doesn't need to be reset
            [posTarget,isReq,taskNum] = checkForReq(requestMatrix,taskNum);  % Check for a request
            if isReq == 1
                triggerComputation = 1;     % checkRetVal is set to 1 to trigger the computation time counter
            end
            counter_check_t = 0;
        end
        counter_check_t = counter_check_t+1;
        
        % Computation time
        if triggerComputation == 1;
            if counter_comp_t == multiple_comp_t
                [idSelRob,pathSelRob] = centrSelRob(sizeMap,posTarget); % For the given map, target position and robot list compute which robot is assigned to complete the task
                currentAssignedTaskMatrix(idSelRob,:) = {pathSelRob,taskNum-1};
                triggerComputation = 0;    % Set checkRetVal back to zero, since the computation to select a robot is done.
                triggerCommunication = 1;     % compRetVal is set to 1 to trigger the communication time counter
                counter_comp_t = -1;    % Reset to -1
            end
            counter_comp_t = counter_comp_t+1;
        end
        
        % Communication time
        if triggerCommunication == 1
            if counter_comm_t == multiple_comm_t
                triggerTravel(idSelRob,1) = 1; % commRetVal is set to 1 to trigger the travel time counter
                triggerCommunication = 0;
                counter_comm_t = -1;
            end
            counter_comm_t = counter_comm_t+1;
        end        
    end
    
    % Travel time
    for i = 1:numRob
        if triggerTravel(i,1) == 1
            if counter_travel_t(i,1) == multiple_travel_t
                [triggerTravel,currentAssignedTaskMatrix,counter_path] = moveRobot1Step(i,currentAssignedTaskMatrix,counter_path,triggerTravel);  % Moves robot to the next cell. robotList is updated to the current position.
                counter_travel_t(i,1) = 0;
            end
            counter_travel_t(i,1) = counter_travel_t(i,1)+1;
        end
        % Copy the current positions of each robot to historyRobotsPositions
        historyRobotsPositions{tot_t+1,i} = robotList(i,1:2);
    end
    
    % Check if no further requests
    if isReq == 0 && sum(triggerTravel) == 0
        tasksCompleted = taskNum-1;
        break
    end    
    
    % Add step_t to the total time for the next iteration
    tot_t = tot_t+step_t;
    
end

display('done')

