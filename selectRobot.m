function [idSelRob,pathSelRob,comp_t,robotWeights] = selectRobot(sizeScenario,posTarget)
% Input: sizeScenario contains the dimension of the quadratic weightMatrix.
%        posTarget contains the position of the target [i,j].
% Output: idSelRob is the ID of the robot with the lowest weight to attend
%         the consumer request on the Robot Availability List. pathSelRob 
%         is a matrix with the i-th row and j-th column of the robot to get 
%         from its current position to the target position.

% Measure the execution time of the function
tic

% Load the Robot Availability List
global robotList;
% i-th row   j-th column   orientation     availability
%     3           18             2                1
%     10           4             3                0
%    ...          ...           ...              ...

alpha = 0.1;  % Scale factor
beta = 1;   % Scale factor

sizeRobotList = size(robotList);
numRob = sizeRobotList(1,1);

% Initialize matrices to store the weights and paths for each robot
robotWeights = NaN(numRob,3);   % G, D, W
robotPaths = num2cell(NaN(numRob,1));

% Compute the weight matrix for the given position of the consumer request (target)
weightMatrix = assignWeigths(sizeScenario,posTarget(1,1),posTarget(1,2));  % weightMatrix is a matrix the size of the scenario with the weights

centerScenario = [sizeScenario/2,sizeScenario/2];
coordCentr = zeros(1,2);

% Compute the weights and paths for each robot and store them in the
% matrices robotWeights and robotPaths. If the robot is not available, the
% matrices contain the value NaN.
for id = 1:numRob
    if robotList(id,4) == 1   % Check if robot i available
        iPosRob = robotList(id,1);
        jPosRob = robotList(id,2);
        orientRob = robotList(id,3);
        [path,G] = navigate(iPosRob,jPosRob,orientRob,posTarget(1,1),posTarget(1,2),weightMatrix,sizeScenario);   % G is the path weight.
        robotList(id,1:2) = [posTarget(1,1),posTarget(1,2)]; % Set the position of the current robot to the target position to compute the new centroid
        [coordCentr(1,1),coordCentr(1,2)] = calcCentroid(numRob,robotList(1:end,1:2));
        D = calcDistance(coordCentr,centerScenario);    % D is the euclidean centroid displacement
        W = alpha*G^2 + beta*D^2;
        robotWeights(id,1:3) = [G,D,W];
        robotPaths{id,1} = path;
        robotList(id,1:2) = [iPosRob,jPosRob];   % Set the position of the current robot back to the current position
    end
end

% Find the robot with the lowest weight
[idSelRob,~] = find(robotWeights == min(robotWeights(:,3)));  % What if several robots have the same D?

% Update the Robot Availability List
robotList(idSelRob,4) = 0;   % Selected Robot is no longer available

% Return the path of the selected robot
pathSelRob = robotPaths{idSelRob,1};

% Computation time of function centrSelRob.m
comp_t = ceil(toc*1000);  % in miliseconds

end