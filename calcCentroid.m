function [ic,jc] = calcCentroid(numRobots,robots)
% Calculates the centroid of the robots

% ic represents the y-coordinate of the centroid
ic = (1/numRobots)*sum(robots(1:end,1)); 

% jc represents the x-coordinate of the centroid
jc = (1/numRobots)*sum(robots(1:end,2));

end

