function [ d ] = calcDistance( c, center )
%Calculates the distance of the centroid from the center of the grid

d=sqrt((c(1,1)-center(1,1))^2+(c(1,2)-center(1,2))^2);


end

