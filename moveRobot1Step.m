function [triggerTravel,currentAssignedTaskMatrix,counter_path,targetReached] = moveRobot1Step(i,currentAssignedTaskMatrix,counter_path,triggerTravel,targetReached)

global robotList

pathSelRob = cell2mat(currentAssignedTaskMatrix(i,1));
robotList(i,1:2) = pathSelRob(counter_path(i,1),:);
counter_path(i,1) = counter_path(i,1)+1;  % Every time moveRobot1Step is called, counter_path increases by 1 to keep track of where the robot is in pathSelRob

if pathSelRob(counter_path(i,1),1) == 0  % pathSelRob was initialized with a bunch of zeros and filled up with the path. Since there is no position on the map with 0 as a coordinate, when the if-expression is true it means the robot has reached the target.
    triggerTravel(i,1) = 0; % The robot has reached the target
    targetReached = 1;      % The robot has reached the target
    robotList(i,4) = 1;  % The robot is now available for the next request. 
    currentAssignedTaskMatrix(i,:) = {zeros(100,2),NaN};
    counter_path(i,1) = 1;
end

end
