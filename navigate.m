function [p, tot_w] = navigate( i_r, j_r, orient, i_targ, j_targ, w, size )
%function for building the path to the person followign the gradient.

global steps
% Retruns p a n x 2 arrary witht the robot positions and tot_w the total
% weight calculated along the path
%Arguments:
%   i_r = inital i position of the robot
%   j_r = initial j position of the robot
%   orient = orientation of the robot
%   i_targ = i position of the target
%   j_targ = j position of the target
%   w = weight matrix
%             up
%             ^
%             |
%   left<----- ------> right
%             |
%             ^
%             down
% 0 = left, 1 = rigth, 2 = up, 3 = down
% i = rows = y
% j = columns = x
p = zeros(100, 2);
i = 1;
j = 1;
tot_w = w(i_r, j_r);
act_w = 0;
%intermediate positions of the robot
i_now = i_r;
j_now = j_r;
orient_now = orient;

p(i, j) = i_r;
p(i, j+1) = j_r;
disp('navigate')
i_now
i_targ
j_now
j_targ
steps(i_r, j_r) = 1100;
i = i+1;

while (i_now ~= i_targ || j_now ~= j_targ)
    if(i_targ >= 5 && i_targ <= 16) %target between the aisles
        %if the robot is stuck in a local minima
        if((w(i_now,j_now) <= w(i_now+1,j_now)) & (w(i_now,j_now) <= w(i_now,j_now+1)) & (w(i_now,j_now) <= w(i_now-1,j_now)) & (w(i_now,j_now) <= w(i_now,j_now-1)))
        disp('Local minima')
             if (i_targ >= 10) %go down
                orient_now = 3;
                while (i_now ~= 17)
                    act_w = w(i_now,j_now);
                    tot_w = tot_w +act_w;
                    p(i,j) = i_now;
                    p(i,j+1) = j_now;
                    steps(i_now,j_now)=1100;
                    i_now = i_now +1;
                    i = i+1;
                end
                orient_now = 3;
                p(i,j) = i_now;
                p(i, j+1) = j_now;
                steps(i_now,j_now)=1100;
            else %go up
                orient_now = 2;
                while (i_now ~= 4)
                    act_w = w(i_now,j_now);
                    tot_w = tot_w +act_w;
                    p(i,j) = i_now;
                    p(i,j+1) = j_now;
                    steps(i_now,j_now)=1100;
                    i_now = i_now -1;
                    i = i+1;
                end
                orient_now = 2;
                p(i,j) = i_now;
                p(i, j+1) = j_now;
                steps(i_now,j_now)=1100;
            end   
        else %free from obstacle           
            switch orient_now
                case 0
                    [i_now, j_now, orient_now, act_w]=navigate_left(i_now, j_now, w, size);
                    tot_w = tot_w + act_w;
                    disp('navigate left')
                case 1
                    [i_now, j_now, orient_now, act_w]=navigate_right(i_now, j_now, w, size);
                    tot_w = tot_w + act_w;
                    disp('navigate right')
                case 2
                    [i_now, j_now, orient_now, act_w]=navigate_up(i_now, j_now, w, size);
                    tot_w = tot_w + act_w;
                    disp('navigate up')
                case 3
                    [i_now, j_now, orient_now, act_w]=navigate_down(i_now, j_now, w, size);
                    tot_w = tot_w + act_w;
                    disp('navigate down')
            end
            p(i, j) = i_now;
            p(i, j+1) = j_now;
            steps(i_now,j_now)=1100;
            i = i+1;
        end
        
    else %person out from the aisle
        disp('switch')
        switch orient_now
            case 0
                [i_now, j_now, orient_now, act_w]=navigate_left(i_now, j_now, w, size);
                tot_w = tot_w + act_w;
                disp('navigate left')
            case 1
                [i_now, j_now, orient_now, act_w]=navigate_right(i_now, j_now, w, size);
                tot_w = tot_w + act_w;
                disp('navigate right')
            case 2
                [i_now, j_now, orient_now, act_w]=navigate_up(i_now, j_now, w, size);
                tot_w = tot_w + act_w;
                disp('navigate up')
            case 3
                [i_now, j_now, orient_now, act_w]=navigate_down(i_now, j_now, w, size);
                tot_w = tot_w + act_w;
                disp('navigate down')
        end
        p(i, j) = i_now;
        p(i, j+1) = j_now;
        steps(i_now,j_now)=1100;
        i = i+1;
    end
    

end
p(i,j)= i_targ;
p(i,j+1)= j_targ;
steps(i_targ,j_targ)=1150;
end

