
function placeObstacle(size, i, j, width, height)
%function for placing obstacles in the environment
%   Put a super big number like 1000 where the obstacle is
% Check if the obstacle is too tall
global a

% while (i-height)<1
%     disp('Obstacle exceeds grid height, size reduced of 1');
%     height = height - 1;
%     disp('New height:');
%     height
% end
% 
% while (i+height)>size
%     disp('Obstacle exceeds grid height, size reduced of 1');
%     height = height - 1;
%     disp('New height:');
%     height
% end
% 
% %Check if the obstacle is too wide
% while (j-width)<1
%     disp('Obstacle exceeds grid width, size reduced of 1');
%     width = width - 1;
%     disp('New width:');
%     width
% end
% 
% while (j+width)>size
%     disp('Obstacle exceeds grid width, size reduced of 1');
%     width = width - 1;
%     disp('New width:');
%     width
% end

% Place the obstacle in the grid
% for j = (j-width):(j+width)
%     for i = i:(i+height)
%     a(i,j) = 1000;
%     end
%     i = i-height;
width = width-1;
height = height-1;
for w = j:(j+width)
    for h = i:(i+height)
    a(h,w) = 1000;
    end
    h = i;
    
end
