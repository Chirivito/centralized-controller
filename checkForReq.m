function [posTarget,isReq,taskNum] = checkForReq(requestMatrix,taskNum)

if taskNum <= size(requestMatrix,1)
    iPosTarget = requestMatrix(taskNum,1);
    jPosTarget = requestMatrix(taskNum,2);
    isReq = 1;
    taskNum = taskNum+1;
else
    iPosTarget = -1;
    jPosTarget = -1;
    isReq = 0;
end

posTarget = [iPosTarget,jPosTarget];

end


