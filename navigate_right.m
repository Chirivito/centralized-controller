function [i_now, j_now, orient_now, act_w ] = navigate_right( i_now, j_now, w, size )
%navigate_right = function for navigation when the robot is facing right

i = i_now;
j = j_now;
w_now = w(i_now, j_now);

%check that I'm not on the border
if (j == 1) %first column
    if (i == 1) %first row
        if w(i, (j+1)) <= w_now
            i_now = i;
            j_now = j+1;
            orient_now = 1;
            act_w = w(i, (j+1));
            return
        else
            i_now = i+1;
            j_now = j;
            orient_now = 3;
            act_w = w((i+1),j);
            return
        end
    elseif (i ~=1 && i ~= size)% not on the first row and not on the last row
        if w(i, (j+1)) <= w_now 
            i_now = i;
            j_now = j+1;
            orient_now = 1;
            act_w = w(i,(j+1));
            return
        elseif w((i-1), j) <= w_now
            i_now = i-1;
            j_now = j;
            orient_now = 2;
           act_w = w((i-1), j);
            return         
        else
            i_now = i+1;
            j_now = j;
            orient_now = 3;
            act_w = w((i+1), j);
            return
        end
    else %on the last row
        if w(i, (j+1)) <= w_now
            i_now = i;
            j_now = j+1;
            orient_now = 1;
            act_w = w(i, (j+1));
            return       
        else
            i_now = i-1;
            j_now = j;
            orient_now = 2;
            act_w = w((i-1), j);
            return
        end
        
    end
    
elseif (j ~= 1 && j ~= size) %not on the first and not on the last column
    if (i == 1) %first row
        if w(i,(j+1)) <= w_now
            i_now = i;
            j_now = j+1;
            orient_now = 1;
            act_w = w(i,(j+1));
            return
        elseif w((i+1), j) <= w_now
            i_now = i+1;
            j_now = j;
            orient_now = 3;
            act_w = w((i+1), j);
            return
        else 
            i_now = i;
            j_now = j-1;
            orient_now = 0;
            act_w = w(i, (j-1));
            return
        end
    elseif(i ~=1 && i ~= size)% not on the first row and not on the last row
         if w(i,(j+1)) <= w_now
            i_now = i;
            j_now = j+1;
            orient_now = 1;
            act_w = w(i,(j+1));
            return
        elseif w((i-1), j) <= w_now
            i_now = i-1;
            j_now = j;
            orient_now = 2;
            act_w = w((i-1), j);
            return
        elseif w((i+1), j) <= w_now
            i_now = i+1;
            j_now = j;
            orient_now = 3;
            act_w = w((i+1), j);
            return
        else 
            i_now = i;
            j_now = j-1;
            orient_now = 0;
            act_w = w(i, (j-1));
            return
         end
    else %on the last row
        if w(i,(j+1)) <= w_now
            i_now = i;
            j_now = j+1;
            orient_now = 1;
            act_w = w(i,(j+1));
            return
        elseif w((i-1), j) <= w_now
            i_now = i-1;
            j_now = j;
            orient_now = 2;
            act_w = w((i-1), j);
            return
        else 
            i_now = i;
            j_now = j-1;
            orient_now = 0;
            act_w = w(i, (j-1));
            return
        end
    end
    
else % on the last column
    if (i == 1) %first row
        if w((i+1),j) <= w_now
            i_now = i+1;
            j_now = j;
            orient_now = 3;
            act_w = w((i+1), j);
            return
        else
            i_now = i;
            j_now = j-1;
            orient_now = 0;
            act_w = w(i,(j-1));
            return
        end
    elseif(i ~=1 && i ~= size)% not on the first row and not on the last row
         if w((i-1),j) <= w_now
            i_now = i-1;
            j_now = j;
            orient_now = 2;
            act_w = w((i-1),j);
            return
        elseif w((i+1), j) <= w_now
            i_now = i+1;
            j_now = j;
            orient_now = 3;
            act_w = w((i+1), j);
            return
         else 
            i_now = i;
            j_now = j-1;
            orient_now = 0;
            act_w = w((i+1), j);
            return
         end
    else %on the last row
        if w((i-1),j) <= w_now
            i_now = i-1;
            j_now = j;
            orient_now = 2;
            act_w = w((i-1),j);
            return
        else 
            i_now = i;
            j_now = j-1;
            orient_now = 0;
            act_w = w(i, (j-1));
            return
        end
    end
      
end



