function w = assignWeigths( size, py, px )
%function for assigning the weights distribution

global a
w = zeros(size); %initialize weights matrix. Same size as the grid
% x = columns = j
% y = rows = i

i = py;
j = px;
weight = 0;
k=1;

if a(i,j) == 1000
    disp('The person cannot be an obstacle! Exit');
    w=a;
    return
end
%w(py, px) = 10;
%fill the column whith the person
while(i >0)
    if (a(i,j) ~= 1000)
        w(i,j) = weight;
        weight = weight +1;    
    else
        w(i,j) = 1000;
        weight = weight+1; %5
        ob = 1;
    end
    i = i-1;
end
weight = 0;
i = py;

while(i <= size)
    if (a(i,j) ~= 1000)
        w(i,j) = weight;
        weight = weight +1;
    else
        w(i,j) = 1000;
        weight = weight+1; %5
    end
    i = i+1;
end
weight = 0;
i = py;

%fill the row with the person
while(j >0)
    if (a(i,j) ~= 1000)
        w(i,j) = weight;
        weight = weight +1;
    else
        w(i,j) = 1000;
        weight = weight+1; %5
    end
    j = j-1;
end
weight = 0;
j = px;

while(j <= size)
    if (a(i,j) ~= 1000)
        w(i,j) = weight;
        weight = weight +1;
    else
        w(i,j) = 1000;
        weight = weight+1; %5
    end
    j = j+1;
end


%fill the rows up/left
n=1;
while (i>=2)
   i = py-n; %move one row up
   j= px-1; %reset to the original column
   if (a(i,j) ~= 1000 && j == px-1)%not an obstacle
   while(w((i+k),j) == 1000) %find the first row free of obstacles
            k = k+1;
        end
        weight=w(i+k,j)+2*k; %0.8
        k=1;
    end
    
    while j>0
        if (a(i,j) ~= 1000)
            w(i,j) = weight;
            weight = weight +1;
        else
            w(i,j) = 1000;
            weight = weight+1; %5
        end 
        j = j-1; %move one row to the left
    end
     n = n+1; % move one row up
end

% %fill the rows down/left
n=1;
i=py;
j=px;
while (i<size)
    i = py+n; %move one row down
    j= px-1; 
    if (a(i,j) ~= 1000 && j == px-1)%not an obstacle
        while(w((i-k),j) == 1000)
            k = k+1;
        end
        weight=w(i-k,j)+2*k; %0.8
        k=1;
    end
    
    while j>0
        if (a(i,j) ~= 1000)
            w(i,j) = weight;
            weight = weight +1;
        else
            w(i,j) = 1000;
            weight = weight+1; %5
        end 
        j = j-1; %move one row to the left
    end
     n = n+1;
end

% %fill the rows up/rigth

n=1;
%reset tp the target position
i=py;
j=px;
while (i>=2)
    i = py-n; %move one row up
    j= px+1; 
    if (a(i,j) ~= 1000 && j ==px+1)%not an obstacle
        while(w((i+k),j) == 1000)
            k = k+1;
        end
        weight=w(i+k,j)+2*k; %0.8
        k=1;
    end
    
    while j<=size
        if (a(i,j) ~= 1000)
            w(i,j) = weight;
            weight = weight +1;
        else
            w(i,j) = 1000;
            weight = weight+1; %5
        end 
        j = j+1; %move one row to the right
    end
     n = n+1;
end

%fill the rows down/right
n=1;
i=py;
j=px;
while (i<size)
    i = py+n; %move one row up
    j= px+1; %reset to the original column
    if (a(i,j) ~= 1000 && j == px+1)%not an obstacle
        while(w((i-k),j) == 1000)
            k = k+1;
        end
        weight=w(i-k,j)+2*k; %0.8
        k=1;
    end
    
    while j<=size
        if (a(i,j) ~= 1000)
            w(i,j) = weight;
            weight = weight +1;
        else
            w(i,j) = 1000;
            weight = weight+1; %5
        end 
        j = j+1; %move one row to the right
    end
     n = n+1;
end


 
end

